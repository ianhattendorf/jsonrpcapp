/*
 *		The MIT License (MIT)
 *
 *
 *		Copyright (c) 2015 Ian Hattendorf
 *
 * 		Permission is hereby granted, free of charge, to any person obtaining a copy
 *		of this software and associated documentation files (the "Software"), to deal
 *		in the Software without restriction, including without limitation the rights
 *		to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *		copies of the Software, and to permit persons to whom the Software is
 *		furnished to do so, subject to the following conditions:
 *
 *		The above copyright notice and this permission notice shall be included in
 *		all copies or substantial portions of the Software.
 *
 *		THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *		IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *		FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *		AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *		LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *		OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *		THE SOFTWARE.
 *
 *		@author Ian Hattendorf mailto:Ian.Hattendorf@asu.edu
 *
 * 		@version February 9, 2015
 */

package edu.asu.bscs.ihattend.jsonrpcapp;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.TextView;

import java.net.URL;

public class CalculateAsync extends AsyncTask<CalcParameters, Integer, Double> {

	private static final String TAG = CalculateAsync.class.getName();
	private JsonRPCCalculator calc;

	private final Context callingContext;

	public CalculateAsync(Context context) {
		callingContext = context;
		calc = new JsonRPCCalculator(context.getString(R.string.service_url));
	}

	@Override
	protected Double doInBackground(CalcParameters... calcParameters) {
		CalcParameters param = calcParameters[0];
		return calc.calculate(param.op1, param.op2, param.operation);
	}

	@Override
	protected void onPostExecute(Double result) {
		Log.d(TAG, "Result: " + result);
		Intent intent = new Intent(MainActivity.CalcEvent);
		intent.putExtra("result", result);
		LocalBroadcastManager.getInstance(callingContext).sendBroadcast(intent);
	}
}
